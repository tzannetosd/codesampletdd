import static org.junit.Assert.*;
import static org.hamcrest.CoreMatchers.instanceOf;
import static org.junit.Assert.assertThat;
import java.util.Collection;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;


public class SocialNetworkTest {

	private SocialNetwork sn;
	private Account me;
	@Before
	public void setUp() throws Exception {
		sn = new SocialNetwork();
		me = sn.join("Hakan");
	}

	@After
	public void tearDown() throws Exception {
		sn.logout();
	}

	private Account getANewFriend(Account account, Account toFriend) throws NoUserLoggedInException {

		sn.login(toFriend);
		sn.sendFriendshipTo(account.getUserName());
		sn.logout();
		sn.login(account);
		sn.acceptFriendshipFrom(toFriend.getUserName());
		sn.logout();
		return toFriend;
	}

	@Test 
	public void whenNoUserIsLoggedInLoggedUserNameShouldBeNull() {
		assertNull(sn.getLoggedUserName());
	}

	@Test 
	public void joiningSocialNetworkWithValidUserNameReturnsTheRightAccount() {
		assertEquals("Hakan", me.getUserName());
	}
	@Test
	public void aRequestToJoinSocialNetworkWithNullUsernameShouldReturnNull() {
		Account her = sn.join(null);
		assertNull(her);
	}

	@Test
	public void aRequestToJoinSocialNetworkWithEmptyUsernameShouldReturnNull() {
		Account her = sn.join("");
		assertNull(her);
	}
	@Test
	public void aRequestToJoinSocialNetworkWithExistingUsernameShouldReturnNull() {
		Account her = sn.join("Hakan");
		assertNull(her);
	}
	@Test 
	public void ifOnlyOneUserJoinsSocialNetworkMemberListShouldContainJustThatUser() {
		try {
			sn.login(me);
			Collection<String> members = sn.listMembers();
			assertEquals(1, members.size());
			assertTrue(members.contains(me.getUserName()));
		}catch(Exception e) {
			fail();
		}
	}

	@Test 
	public void ifTwoUsersJoinSocialNetworkMemberListShouldContainBoth() {
		try {
			sn.join("Cecile");
			sn.login(me);
			Collection<String> members = sn.listMembers();
			assertEquals(2, members.size());
			assertTrue(members.contains("Hakan"));
			assertTrue(members.contains("Cecile"));
		}catch(Exception e) {
			fail();
		}
	}


	@Test
	public void aNewAccountDoesntHaveAnyPendingIncomingRequests() {
		assertTrue(me.getIncomingRequests().isEmpty());
	}

	@Test
	public void aNewAccountDoesntHaveAnyPendingOutgoingRequests() {
		assertTrue(me.getOutgoingRequests().isEmpty());
	}

	@Test
	public void aLoggedInMemberCanAssessSocialNetwork() {
		Account member = sn.login(me);
		assertEquals(member.getUserName(), sn.getLoggedUserName());
	}
	@Test
	public void aMemberShouldHaveJoinedSocialNetworkBeforeLoggingIn() {
		Account her = new Account("Cecile");
		Account test = sn.login(her);
		assertNull(test);
	}
	
	@Test 
	public void aMemberCanSendAFriendRequestToAnother() {
		try {
			Account her = sn.join("Cecile");
			Account logged = sn.login(me);
			assertNotNull(me);
			assertNotNull(her);
			sn.sendFriendshipTo("Cecile");
			assertTrue(her.getIncomingRequests().contains(sn.getLoggedUserName()));
			assertTrue(logged.getOutgoingRequests().contains(her.getUserName()));
		}catch(Exception e) {
			fail();
		}
	}
	

	@Test
	public void aLoggedInMemberCannotAccessSocialNetworkAfterLogout() {
		sn.login(me);
		sn.logout();
		assertNull(sn.getLoggedUserAccount());
	}

	@Test 
	public void aMemberCanAcceptAFriendRequestFromAnother() {
		try {
			Account mary = sn.join("Mary");
			Account logged = sn.login(mary);
			sn.sendFriendshipTo(me.getUserName());
			sn.logout();
			logged = sn.login(me);
			sn.acceptFriendshipFrom(mary.getUserName());
			assertTrue(mary.hasFriend(me.getUserName()));
			assertTrue(me.hasFriend(mary.getUserName()));
		}catch(Exception e) {
			fail();
		}
	}

	@Test
	public void aMemberShouldBeAbleToAcceptAllFriendRequestsAtOnce() {
		try {
			Account her = sn.join("Cecile");
			Account logged = sn.login(her);
			sn.sendFriendshipTo(me.getUserName());
			sn.logout();
			Account mary = sn.join("Mary");
			logged = sn.login(mary);
			sn.sendFriendshipTo(me.getUserName());
			sn.logout();
			logged = sn.login(me);
			sn.acceptAllFriendships();
			assertEquals(2, me.getFriends().size());
			assertTrue(me.hasFriend(her.getUserName()));
			assertTrue(her.hasFriend(me.getUserName()));
			assertFalse(me.getIncomingRequests().contains(her.getUserName()));
			assertFalse(her.getOutgoingRequests().contains(me.getUserName()));
			assertTrue(me.hasFriend(mary.getUserName()));
			assertTrue(mary.hasFriend(me.getUserName()));
		}catch(Exception e) {
			fail();
		}
	}

	@Test
	public void aMemberCanRejectAFriendRequestFromAnother() {
		try {
			Account her = sn.join("Cecile");
			Account logged = sn.login(her);
			sn.sendFriendshipTo(me.getUserName());
			sn.logout();
			logged = sn.login(me);
			sn.rejectFriendshipFrom(her.getUserName());
			assertFalse(logged.getIncomingRequests().contains(sn.getLoggedUserName()));
			assertFalse(her.getOutgoingRequests().contains(her.getUserName()));
			assertFalse(me.hasFriend(her.getUserName()));
			assertFalse(her.hasFriend(me.getUserName()));
		}catch(Exception e) {
			fail();
		}
	}

	@Test
	public void aMemberShouldBeAbleToRejectAllFriendRequestsAtOnce() {
		try {
			Account her = sn.join("Cecile");
			Account logged = sn.login(her);
			sn.sendFriendshipTo(me.getUserName());
			sn.logout();
			Account mary = sn.join("Mary");
			logged = sn.login(mary);
			sn.sendFriendshipTo(me.getUserName());
			sn.logout();
			logged = sn.login(me);
			sn.rejectAllFriendships();
			assertFalse(me.getIncomingRequests().contains(her.getUserName()));
			assertFalse(me.getIncomingRequests().contains(mary.getUserName()));
			assertFalse(her.getOutgoingRequests().contains(me.getUserName()));
			assertFalse(mary.getOutgoingRequests().contains(me.getUserName()));
		}catch(Exception e) {
			fail();
		}
	}

	@Test
	public void aLoggedInMemberCanFindOutIfAnExistingUserIsMember() {
		try {
			Account her = sn.join("Cecile");
			sn.login(me);
			assertTrue(sn.hasMember(her.getUserName()));
		}catch(Exception e) {
			fail();
		}
	}
	@Test
	public void aLoggedInMemberCanFindOutIfANonExistingUserIsMember() {
		try {
			sn.login(me);
			assertFalse(sn.hasMember("Cecile"));
		}catch(Exception e) {
			fail();
		}
	}

	@Test
	public void aMemberShouldBeAbleToAutoAcceptNewFriendRequests() {
		try {
			Account her = sn.join("Cecile");
			sn.login(me);
			sn.autoAcceptFriendships();
			sn.logout();
			sn.login(her);
			sn.sendFriendshipTo(me.getUserName());
			assertEquals(1, me.getFriends().size());
			assertTrue(me.hasFriend(her.getUserName()));
			assertTrue(her.hasFriend(me.getUserName()));
		}catch(Exception e) {
			fail();
		}

	}

	@Test
	public void aMemberShouldBeAbleToCancelAutoAcceptNewFriendRequests() {
		try {
			Account her = sn.join("Cecile");
			sn.login(me);
			sn.autoAcceptFriendships();
			sn.cancelAutoAcceptFriendships();
			sn.logout();
			sn.login(her);
			sn.sendFriendshipTo(me.getUserName());
			assertEquals(0, me.getFriends().size());
			assertFalse(me.hasFriend(her.getUserName()));
			assertFalse(her.hasFriend(me.getUserName()));
			assertTrue(me.getIncomingRequests().contains(her.getUserName()));
			assertTrue(her.getOutgoingRequests().contains(me.getUserName()));
		}catch(Exception e) {
			fail();
		}

	}

	@Test
	public void aMemberShouldBeAbleToUnfriendAnExistingFriend() {
		try {
			Account mary = sn.join("Mary");
			Account logged = sn.login(mary);
			sn.sendFriendshipTo(me.getUserName());
			sn.logout();
			logged = sn.login(me);
			sn.acceptFriendshipFrom(mary.getUserName());
			sn.sendFriendshipCancellationTo(mary.getUserName());
			assertFalse(me.hasFriend(mary.getUserName()));
			assertFalse(mary.hasFriend(me.getUserName()));
		}catch(Exception e) {
			fail();
		}
	}

	@Test
	public void aMemberThatLeftisNotInMemberlistAnymore() {
		try {
			Account her = sn.join("Cecile");
			sn.login(me);
			sn.leave();
			sn.login(her);
			Collection<String> members = sn.listMembers();
			assertEquals(1, members.size());
			assertFalse(members.contains(me.getUserName()));
		}catch(Exception e) {
			fail();
		}
	}


	@Test
	public void aMemberWithFriendsWhenLeftHasNoMoreFriends() {
		try {
			Account her = sn.join("Cecile");
			getANewFriend(me, her);
			sn.login(me);
			sn.leave();
			assertTrue(me.getFriends().isEmpty());
		}catch(Exception e) {
			fail();
		}
	}

	@Test
	public void theFriendsOfAMemberThatLeftAreNoLongerFriendsWithThatMember() {
		try {
			Account her = sn.join("Cecile");
			Account friend = getANewFriend(me, her);
			sn.login(me);
			sn.leave();
			assertFalse(friend.hasFriend(me.getUserName()));
		}catch(Exception e) {
			fail();
		}
	}

	@Test
	public void aMemberThatLeftHasNoIncomingRequests() {
		try {
			Account her = sn.join("Cecile");
			sn.login(her);
			sn.sendFriendshipTo(me.getUserName());
			sn.logout();
			sn.login(me);
			sn.leave();
			assertTrue(me.getIncomingRequests().isEmpty());
			assertTrue(her.getOutgoingRequests().isEmpty());
		}catch(Exception e) {
			fail();
		}
	}

	@Test
	public void aMemberThatLeftHasNoOutgoingRequests() {
		try {
			Account her = sn.join("Cecile");
			sn.login(me);
			sn.sendFriendshipTo(her.getUserName());
			sn.leave();
			assertTrue(her.getIncomingRequests().isEmpty());
			assertTrue(me.getOutgoingRequests().isEmpty());
		}catch(Exception e) {
			fail();
		}
	}

	@Test
	public void aMemberIsNotVisibleFromBlockedUser() {
		try {
			Account her = sn.join("Cecile");
			sn.login(me);
			sn.block(her.getUserName());
			sn.logout();
			sn.login(her);
			assertFalse(sn.hasMember(me.getUserName()));
		}catch(Exception e) {
			fail();
		}
	}
	@Test
	public void aMemberIsNotInABlockedUserList() {
		try {
			Account her = sn.join("Cecile");
			sn.login(me);
			sn.block(her.getUserName());
			sn.logout();
			sn.login(her);
			assertFalse(sn.listMembers().contains((me.getUserName())));
		}catch(Exception e) {
			fail();
		}
	}

	@Test
	public void aMemberCannotReceiveFriendRequestFromBlockedUser() {
		try {
			Account her = sn.join("Cecile");
			sn.login(me);
			sn.block(her.getUserName());
			sn.logout();
			sn.login(her);
			sn.sendFriendshipTo(me.getUserName());
			assertTrue(me.getIncomingRequests().isEmpty());
			assertTrue(her.getOutgoingRequests().isEmpty());
		}catch(Exception e) {
			fail();
		}
	}

	@Test
	public void aMemberIsAgainVisibleFromPreviouslyBlockedUser() {
		try {
			Account her = sn.join("Cecile");
			sn.login(me);
			sn.block(her.getUserName());
			sn.unblock(her.getUserName());
			sn.logout();
			sn.login(her);
			assertTrue(sn.hasMember(me.getUserName()));
		}catch(Exception e) {
			fail();
		}
	}

	@Test
	public void aMemberIsInAPreviouslyBlockedUserList() {
		try {
			Account her = sn.join("Cecile");
			sn.login(me);
			sn.block(her.getUserName());
			sn.unblock(her.getUserName());
			sn.logout();
			sn.login(her);
			assertTrue(sn.listMembers().contains((me.getUserName())));
		}catch(Exception e) {
			fail();
		}
	}

	@Test
	public void aMemberCanReceiveAgainFriendRequestFromPreviouslyBlockedUser() {
		try {
			Account her = sn.join("Cecile");
			sn.login(me);
			sn.block(her.getUserName());
			sn.unblock(her.getUserName());
			sn.logout();
			sn.login(her);
			sn.sendFriendshipTo(me.getUserName());
			assertTrue(me.getIncomingRequests().contains(her.getUserName()));
			assertTrue(her.getOutgoingRequests().contains(me.getUserName()));
		}catch(Exception e) {
			fail();
		}
	}

	@Test
	public void aMemberWithFriendShouldBeRecommendedFriendsofFriends() {
		try {
			Account cecile = sn.join("Cecile");
			Account bob = sn.join("Bob");
			Account john = sn.join("John");
			Account mary = sn.join("Mary");
			Account mike = sn.join("Mike");
			getANewFriend(me, cecile);
			getANewFriend(me, bob);
			getANewFriend(bob,john);
			getANewFriend(bob, mary);
			getANewFriend(bob, mike);
			getANewFriend(cecile,john);
			getANewFriend(cecile, mary);
			getANewFriend(cecile, mike);
			sn.login(me);
			Collection<String> recom = sn.recommendFriends();
			assertEquals(3, recom.size());
			assertTrue(recom.contains(john.getUserName()));
			assertTrue(recom.contains(mary.getUserName()));
			assertTrue(recom.contains(mike.getUserName()));
		}catch(Exception e) {
			fail();
		}
	}
	
	@Test
	public void aMemberWithNoFriendShouldNotBeRecommendedAnyFriend() {
		try { 
			sn.login(me);
			assertTrue(sn.recommendFriends().isEmpty());
		}catch(Exception e) {
			fail();
		}
	}

	@Test
	public void NoRecommendationExists() {
		try {
			Account cecile = sn.join("Cecile");
			Account bob = sn.join("Bob");
			Account john = sn.join("John");
			Account mike = sn.join("Mike");

			getANewFriend(me, cecile);
			getANewFriend(me, bob);
			getANewFriend(bob,john);
			getANewFriend(cecile, mike);
			sn.login(me);
			Collection<String> recom = sn.recommendFriends();
			assertTrue(recom.isEmpty());
		}catch(Exception e) {
			fail();
		}
	}

	@Test
	public void whenAMemberBlocksAnExistingUserTheyAreNoLongerFriends() {
		try {
			Account cecile = sn.join("Cecile");
			getANewFriend(me, cecile);
			sn.login(me);
			sn.block(cecile.getUserName());
			assertFalse(me.hasFriend(cecile.getUserName()));
			assertFalse(cecile.hasFriend(me.getUserName()));
		}catch(Exception e) {
			fail();
		}
	}

	@Test
	public void anIncomingFriendRequestFromUserGetsDeletedWhenMemberBlocksUser() {
		try {
			Account cecile = sn.join("Cecile");
			sn.login(cecile);
			sn.sendFriendshipTo(me.getUserName());
			sn.login(me);
			sn.block(cecile.getUserName());
			assertFalse(me.getIncomingRequests().contains(cecile.getUserName()));
			assertFalse(cecile.getOutgoingRequests().contains(me.getUserName()));
		}catch(Exception e) {
			fail();
		}
	}

	@Test
	public void anOutgoingFriendRequestToUserGetsDeletedWhenMemberBlocksUser() {
		try {
			Account cecile = sn.join("Cecile");
			sn.login(me);
			sn.sendFriendshipTo(cecile.getUserName());
			sn.block(cecile.getUserName());
			sn.logout();
			assertFalse(me.getOutgoingRequests().contains(cecile.getUserName()));
			assertFalse(cecile.getIncomingRequests().contains(me.getUserName()));
		}catch(Exception e) {
			fail();
		}
	}

	@Test
	public void aUserMustBeLoggedInToPerformOperations() {
		Account her = sn.join("Cecile");
		try {
			sn.listMembers();
		}catch(Exception e) {
			assertThat(e, instanceOf(NoUserLoggedInException.class));
		}
		try {
			sn.acceptAllFriendships();;
		}catch(Exception e) {
			assertThat(e, instanceOf(NoUserLoggedInException.class));
		}
		try {
			sn.acceptFriendshipFrom(her.getUserName());;
		}catch(Exception e) {
			assertThat(e, instanceOf(NoUserLoggedInException.class));
		}
		try {
			sn.autoAcceptFriendships();
		}catch(Exception e) {
			assertThat(e, instanceOf(NoUserLoggedInException.class));
		}
		try {
			sn.block(her.getUserName());
		}catch(Exception e) {
			assertThat(e, instanceOf(NoUserLoggedInException.class));
		}
		try {
			sn.cancelAutoAcceptFriendships();
		}catch(Exception e) {
			assertThat(e, instanceOf(NoUserLoggedInException.class));
		}
		try {
			sn.hasMember(her.getUserName());
		}catch(Exception e) {
			assertThat(e, instanceOf(NoUserLoggedInException.class));
		}
		try {
			sn.leave();
		}catch(Exception e) {
			assertThat(e, instanceOf(NoUserLoggedInException.class));
		}
		try {
			sn.listMembers();
		}catch(Exception e) {
			assertThat(e, instanceOf(NoUserLoggedInException.class));
		}
		try {
			sn.recommendFriends();
		}catch(Exception e) {
			assertThat(e, instanceOf(NoUserLoggedInException.class));
		}
		try {
			sn.rejectAllFriendships();
		}catch(Exception e) {
			assertThat(e, instanceOf(NoUserLoggedInException.class));
		}
		try {
			sn.rejectAllFriendshipsFrom();
		}catch(Exception e) {
			assertThat(e, instanceOf(NoUserLoggedInException.class));
		}
		try {
			sn.sendFriendshipCancellationTo(her.getUserName());
		}catch(Exception e) {
			assertThat(e, instanceOf(NoUserLoggedInException.class));
		}
		try {
			sn.sendFriendshipCancellationTo(her.getUserName());
		}catch(Exception e) {
			assertThat(e, instanceOf(NoUserLoggedInException.class));
		}
		try {
			sn.unblock(her.getUserName());
		}catch(Exception e) {
			assertThat(e, instanceOf(NoUserLoggedInException.class));
		}
	}
}
