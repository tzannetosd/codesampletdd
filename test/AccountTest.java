import static org.junit.Assert.*;

import org.junit.Before; 
import org.junit.Test;
 


public class AccountTest {

	Account me, her, another;

	@Before
	public void setUp() throws Exception {
		me = new Account("Hakan");
		her = new Account("Serra");
		another = new Account("Cecile");
	}

	@Test
	public void aMemberCanReceiveAFriendRequestFromAnother() {
		me.requestFriendship(her);
		assertTrue(me.getIncomingRequests().contains(her.getUserName()));
		assertTrue(her.getOutgoingRequests().contains(me.getUserName()));
	}

	@Test
	public void aMemberCanSendAFriendRequestToAnother() {
		me.requestFriendship(her);
		assertTrue(her.getOutgoingRequests().contains(me.getUserName()));
	}


	@Test
	public void aMemberHasInitiallyNoIncomingFriendRequests() {
		assertEquals(0, me.getIncomingRequests().size());
	}

	@Test
	public void aMemberCanReceiveFriendRequestFromMultipleMembers() {
		me.requestFriendship(her);
		me.requestFriendship(another);
		assertEquals(2, me.getIncomingRequests().size());
		assertTrue(me.getIncomingRequests().contains(another.getUserName()));
		assertTrue(me.getIncomingRequests().contains(her.getUserName()));
	}

	@Test
	public void multipleFriendRequestsToSamePersonAreCountedOnce() {
		me.requestFriendship(her);
		me.requestFriendship(her);
		assertEquals(1, me.getIncomingRequests().size());
	}
 
	@Test
	public void anIncomingFriendRequestIsRemovedFromListAfterMemberAccepts() {
		me.requestFriendship(her);
		her.friendshipAccepted(me);
		assertFalse(me.getIncomingRequests().contains(her.getUserName()));
	}

	@Test
	public void anIncomingFriendRequestIsRemovedFromListAfterMemberRejects() {
		me.requestFriendship(her);
		her.friendshipRejected(me);
		assertFalse(me.getIncomingRequests().contains(her.getUserName()));
	}

	@Test
	public void aMemberIsFriendsWithAnotherWhenAnotherAcceptsFriendRequest() {
		me.requestFriendship(her);
		her.friendshipAccepted(me);
		assertTrue(me.hasFriend(her.getUserName()));
		assertTrue(her.hasFriend(me.getUserName()));
	}

	@Test
	public void aMemberIsNotFriendsWithAnotherWhenAnotherRejectsFriendRequest() {
		me.requestFriendship(her);
		her.friendshipRejected(me);
		assertFalse(me.hasFriend(her.getUserName()));
		assertFalse(her.hasFriend(me.getUserName()));
	}

	@Test
	public void anOutgoingFriendRequestIsRemovedFromListAfterMemberAccepts() {
		me.requestFriendship(her);
		her.friendshipAccepted(me);
		assertFalse(her.getOutgoingRequests().contains(me.getUserName()));
	}
	@Test
	public void anOutgoingFriendRequestIsRemovedFromListAfterMemberRejects() {
		me.requestFriendship(her);
		her.friendshipRejected(me);
		assertFalse(her.getOutgoingRequests().contains(me.getUserName()));
	}

	@Test
	public void threeWayFriendshipIsPossible() {
		me.requestFriendship(her);
		me.requestFriendship(another);
		her.requestFriendship(another);
		her.friendshipAccepted(me);
		another.friendshipAccepted(her);
		another.friendshipAccepted(me);
		assertTrue(me.hasFriend(her.getUserName()));
		assertTrue(me.hasFriend(another.getUserName()));
		assertTrue(her.hasFriend(me.getUserName()));
		assertTrue(her.hasFriend(another.getUserName()));
		assertTrue(another.hasFriend(her.getUserName()));
		assertTrue(her.hasFriend(me.getUserName()));
	}

	@Test
	public void cannotBeFriendsWithAnExistingFriend() {
		me.requestFriendship(her);
		her.friendshipAccepted(me);
		assertTrue(her.hasFriend(me.getUserName()));
		me.requestFriendship(her);
		assertFalse(me.getIncomingRequests().contains(her.getUserName()));
		assertFalse(her.getIncomingRequests().contains(me.getUserName()));
	}

	@Test
	public void autoAcceptFriendRequestWhenNoIncomingRequestsArePending() {
		me.autoAcceptFriendships();
		me.requestFriendship(her);
		assertFalse(me.getIncomingRequests().contains(her.getUserName()));
		assertTrue(me.hasFriend(her.getUserName()));
		assertTrue(her.hasFriend(me.getUserName()));
	}

	@Test
	public void autoAcceptFriendRequestWhenIncomingRequestsArePending() {
		me.requestFriendship(her);
		assertTrue(me.getIncomingRequests().contains(her.getUserName()));
		me.autoAcceptFriendships();
		me.requestFriendship(another);
		assertFalse(me.getIncomingRequests().contains(another.getUserName()));
		assertTrue(me.hasFriend(another.getUserName()));
		assertTrue(another.hasFriend(me.getUserName()));
		assertFalse(me.hasFriend(her.getUserName()));
		assertFalse(her.hasFriend(me.getUserName()));
	}

	@Test
	public void unFriendAnExistingFriend() {
		me.requestFriendship(her);
		her.friendshipAccepted(me);
		her.cancelFriendship(me);
		assertFalse(her.hasFriend(me.getUserName()));
		assertFalse(me.hasFriend(her.getUserName()));
	}
	
	@Test
	public void aFriendRequestFromNullAccountHasNoEffect() {
		me.requestFriendship(null);
		assertTrue(me.getIncomingRequests().isEmpty());
	}
	
	@Test
	public void receivingFriendAcceptWhenToAccountIsNullHasNoEffect() {
		me.friendshipAccepted(null);
		assertTrue(me.getOutgoingRequests().isEmpty());
	}
	
	@Test
	public void aFriendRequestFromAlreadyFriendHasNoEffect() {
		me.requestFriendship(her);
		her.friendshipAccepted(me);
		me.requestFriendship(her);
		assertTrue(me.getIncomingRequests().isEmpty());
		assertTrue(her.getOutgoingRequests().isEmpty());
	}
	
	@Test
	public void rejectingFriendshipWhenNoFriendRequestIsSentHasNoEffect() {
		her.friendshipRejected(me);
		assertFalse(me.hasFriend(her.getUserName()));
		assertFalse(her.hasFriend(me.getUserName()));
	}	 
}
