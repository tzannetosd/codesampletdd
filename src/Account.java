import java.util.HashSet;
import java.util.Set;


public class Account  {

	// the unique user name of account owner
	private String userName;
	private boolean autoAcceptFriendRequests = false;
	// list of members who are awaiting an acceptance response from this account's owner 
	private Set<String> incomingRequests = new HashSet<String>();
	private Set<String> outgoingRequests = new HashSet<String>();
	private Set<String> blockList = new HashSet<String>();
	// list of members who are friends of this account's owner
	private Set<String> friends = new HashSet<String>();

	public Account(String userName) {
		this.userName = userName;
	}

	public String getUserName() {
		return userName;
	}

	// return list of members who had sent a friend request to this account's owner 
	// and are still waiting for a response
	public Set<String> getIncomingRequests() {
		return incomingRequests; 
	}
	// return list of members to whom this account's owner has sent a friend request 
	// and are still waiting for a response
	public Set<String> getOutgoingRequests() {
		return outgoingRequests; 
	}

	// an incoming friend request to this account's owner from another member account
	public void requestFriendship(Account fromAccount) {
		if (fromAccount != null) {
			if (!friends.contains(fromAccount.getUserName())) {
				incomingRequests.add(fromAccount.getUserName());
			}
			if (!fromAccount.friends.contains(this.getUserName())) {
				fromAccount.outgoingRequests.add(this.getUserName());
			}
			if (this.autoAcceptFriendRequests) {
				fromAccount.friendshipAccepted(this);
			}
		} 

	}

	// check if account owner has a member with user name userName as a friend
	public boolean hasFriend(String userName) {
		return friends.contains(userName);
	}

	// receive an acceptance from a member to whom a friend request has been sent and from whom no response has been received
	public void friendshipAccepted(Account toAccount) {
		if (toAccount != null &&
				!this.friends.contains(toAccount.getUserName()) &&
				!toAccount.friends.contains(this.getUserName()) &&
				this.outgoingRequests.contains(toAccount.getUserName())){
			friends.add(toAccount.getUserName());
			toAccount.friends.add(this.getUserName());
			toAccount.incomingRequests.remove(this.getUserName());
			outgoingRequests.remove(toAccount.getUserName());
		}

	}
	// receive a rejection from a member to whom a friend request has been sent and from whom no response has been received
	public void friendshipRejected(Account toAccount) {
		if (toAccount != null) {
			toAccount.incomingRequests.remove(this.getUserName());
			outgoingRequests.remove(toAccount.getUserName());
		}
	}

	public Set<String> getFriends() {
		return friends;
	}

	public void autoAcceptFriendships() {
		this.autoAcceptFriendRequests = true;
	}
	
	public void cancelAutoAcceptFriendships() {
		this.autoAcceptFriendRequests = false;
	}
	
	// remove a member from this account's owner friend list
	public void cancelFriendship(Account toAccount) {
		if (toAccount != null) {
			friends.remove(toAccount.getUserName());
			toAccount.friends.remove(this.getUserName());
		}
	}
	
	public void blockUser(String userName) {
		this.blockList.add(userName);
	}
	
	public void unblockUser(String userName) {
		this.blockList.remove(userName);
	}

	public boolean hasBlocked(String userName) {
		return this.blockList.contains(userName);
	}
}
