import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;
import java.util.Collection;

public class SocialNetwork implements ISocialNetwork{

	private Collection<Account> accounts = new HashSet<Account>();
	private Account loggedUser;
	// join SN with a new user name
	public Account join(String userName) {
		if (userName == null || userName.equals("")) {
			return null;
		}
		else if (accounts.contains(findAccountForUserName(userName))){
			return null;
		}
		Account newAccount = new Account(userName);
		accounts.add(newAccount);
		return newAccount;
	}

	// find a member by user name 
	private Account findAccountForUserName(String userName) {
		// find account with user name userName
		// not accessible to outside because that would give a user full access to another member's account
		for (Account each : accounts) {
			if (each.getUserName().equals(userName)) 
				return each;
		}
		return null;
	}

	private void ensureLoggedInUser() throws NoUserLoggedInException {
		if (this.loggedUser == null) {
			throw new NoUserLoggedInException();
		}
	}

	// list user names of all members
	public Collection<String> listMembers() throws NoUserLoggedInException {
		ensureLoggedInUser();
		Collection<String> members = new HashSet<String>();
		for (Account each : accounts) {
			if (!each.hasBlocked(this.getLoggedUserName())) {
				members.add(each.getUserName());
			}
		}
		return members;
	}


	@Override
	public Account login(Account me) {
 
		if (this.findAccountForUserName(me.getUserName()) == null) {
			return null;
		}
		this.loggedUser = me;
		return this.loggedUser;
	}

	@Override
	public void logout() {
		this.loggedUser = null;
	}

	@Override
	public boolean hasMember(String userName) throws NoUserLoggedInException{
		ensureLoggedInUser();
		Account accountForUserName = findAccountForUserName(userName);
		if (accountForUserName == null) {
			return false;
		}
		if (accountForUserName.hasBlocked(this.getLoggedUserName())) {
			return false;
		}
		return true;
	}

	@Override
	// from my account, send a friend request to user with userName from my account
	public void sendFriendshipTo(String userName) throws NoUserLoggedInException {
		ensureLoggedInUser();
		Account accountForUserName = findAccountForUserName(userName);
		if (accountForUserName != null && !accountForUserName.hasBlocked(this.getLoggedUserName())) {
			accountForUserName.requestFriendship(this.getLoggedUserAccount());
		}
	}

	@Override
	public void block(String userName) throws NoUserLoggedInException {
		ensureLoggedInUser();
		Account accountForUserName = findAccountForUserName(userName);
		Account loggedUser = this.getLoggedUserAccount();
		if (accountForUserName != null) {
			loggedUser.blockUser(userName);
		}
		if (loggedUser.hasFriend(userName)) {
			accountForUserName.cancelFriendship(loggedUser);
		}
		if (loggedUser.getIncomingRequests().contains(userName)) {
			accountForUserName.friendshipRejected(loggedUser);
		}
		if (loggedUser.getOutgoingRequests().contains(userName)) {
			loggedUser.friendshipRejected(accountForUserName);
		}
	}

	@Override
	public void unblock(String userName) throws NoUserLoggedInException {
		ensureLoggedInUser();
		Account accountForUserName = findAccountForUserName(userName);
		if (accountForUserName != null) {
			this.getLoggedUserAccount().unblockUser(userName);
		}
	}

	// cancel existing friendship with user defined by userName
	@Override
	public void sendFriendshipCancellationTo(String userName) throws NoUserLoggedInException {
		ensureLoggedInUser();
		Account her = findAccountForUserName(userName);
		if (her != null) {
			her.cancelFriendship(this.getLoggedUserAccount());
		}

	}

	// accept a pending friend request from another user with userName
	public void acceptFriendshipFrom(String userName) throws NoUserLoggedInException {
		ensureLoggedInUser();
		Account accountForUserName = findAccountForUserName(userName);
		if (accountForUserName != null) {
			accountForUserName.friendshipAccepted(this.getLoggedUserAccount());
		}

	}
	// returns a list of all pending incoming requests to my account
	private Collection<String> getIncomingRequestNameTo() throws NoUserLoggedInException {
		ensureLoggedInUser();
		Collection<String> names = new HashSet<String>();
		for (String name : this.getLoggedUserAccount().getIncomingRequests()) {
			names.add(name);
		}
		return names;

	}
	// returns a list of all pending outgoing requests from my account
	private Collection<String> getOutgoingRequestNameFrom() throws NoUserLoggedInException {
		ensureLoggedInUser();
		Collection<String> names = new HashSet<String>();
		for (String name : this.getLoggedUserAccount().getOutgoingRequests()) {
			names.add(name);
		}
		return names;


	}

	//accept all pending friend requests
	@Override
	public void acceptAllFriendships() throws NoUserLoggedInException {
		ensureLoggedInUser();
		for (String userName : getIncomingRequestNameTo()) {
			acceptFriendshipFrom(userName);
		}
	}

	//  reject a pending friend request from another user with userName
	public void rejectFriendshipFrom(String userName) throws NoUserLoggedInException {
		ensureLoggedInUser();
		Account her = findAccountForUserName(userName);
		her.friendshipRejected(this.getLoggedUserAccount());

	}

	// cancel all pending friend request sent FROM my account
	public void rejectAllFriendshipsFrom() throws NoUserLoggedInException {
		ensureLoggedInUser();
		for (String userName : getOutgoingRequestNameFrom()) {
			Account her = findAccountForUserName(userName);
			this.getLoggedUserAccount().friendshipRejected(her);
		}
	}

	// cancel all pending friend request sent TO my account
	@Override
	public void rejectAllFriendships() throws NoUserLoggedInException {
		ensureLoggedInUser();
		for (String userName : getIncomingRequestNameTo()) {
			rejectFriendshipFrom(userName);
		}
	}

	// enable auto-accepting *new* friend requests
	@Override
	public void autoAcceptFriendships() throws NoUserLoggedInException {
		ensureLoggedInUser();
		if (this.getLoggedUserAccount() != null) {
			this.getLoggedUserAccount().autoAcceptFriendships();
		}
	}

	// disable auto-accepting *new* friend requests
	@Override
	public void cancelAutoAcceptFriendships() throws NoUserLoggedInException {
		ensureLoggedInUser();
		// TODO Auto-generated method stub
		if (this.getLoggedUserAccount() != null) {
			this.getLoggedUserAccount().cancelAutoAcceptFriendships();
		}
	}

	@Override
	public Collection<String> recommendFriends() throws NoUserLoggedInException {
		ensureLoggedInUser();
		Collection<String> rec = new HashSet<String>();
		Collection<String> myFriends = this.getLoggedUserAccount().getFriends();
		for (Account acc : accounts) {
			if (!myFriends.contains(acc.getUserName()) && !acc.getUserName().equals(this.getLoggedUserName())) {
				 
				Collection<String> friends = acc.getFriends();
				int conn = 0;
				for(String f : friends) {
					if (myFriends.contains(f)) {
						conn++;
					}
				}
				if (conn > 1) {
					rec.add(acc.getUserName());
				}
			}
		}

		return rec;
	}

	// delete my account from Social Network
	@Override
	public void leave() throws NoUserLoggedInException {
		ensureLoggedInUser();
		Account me = this.getLoggedUserAccount();

		rejectAllFriendships();
		rejectAllFriendshipsFrom();
		for (Account friend : getMyFriendsAccountList()) {
			friend.cancelFriendship(me);
		}
		accounts.remove(me);
		logout();
	}

	private Collection<Account> getMyFriendsAccountList() throws NoUserLoggedInException {
		ensureLoggedInUser();
		Account me = this.getLoggedUserAccount();
		Collection<Account> accounts = new HashSet<Account>();
		for (String friend : me.getFriends()) {
			Account acc = findAccountForUserName(friend);
			accounts.add(acc);
		}
		return accounts;
	}

	public String getLoggedUserName() {
		if (this.loggedUser != null)
			return this.loggedUser.getUserName();
		return null;
	}

	public Account getLoggedUserAccount() {
		return this.loggedUser;
	}
}
